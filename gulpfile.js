/**
 * Gulp-задания для сборщика
 *
 * Необходимые плагины:
 *     gulp
 *     gulp-util
 *     gulp-plumber
 *     path
 *     gulp-less
 *     gulp-watch-less
 *     gulp-concat
 *     gulp-clean-css
 *     gulp-uglify
 *     gulp-rename
 *     gulp-sourcemaps
 *     browserify
 *     babelify
 *     vinyl-source-stream
 *     vinyl-buffer
 *     watchify
 *     lodash.assign
 *
 *     babel-preset-es2015
 *     babel-plugin-transform-es2015-classes
 *
 *     gulp-postcss
 *     postcss-nested
 *     postcss-mixins
 *     postcss-simple-vars
 *     postcss-easings
 *     postcss-import
 *     postcss-cssnext
 *
 * Задания:
 *     watch-less - наблюдатель за изменением LESS файлов и их сборкой
 *     watch-postcss - наблюдатель за изменением PostCSS файлов и их сборкой
 *     watch-es6 - наюблюдатель за изменениями в ES6 файлах
 *     build - полная сборка проекта
 *         build-with-postcss - сборка и минимизация PostCSS файлов
 *            production - переключение в режим production
 *            postcss - компиляция PostCSS
 *         build-js - сборка и минимизация JS файлов
 *            production - переключение в режим production
 *            build-es6 - сборка и компиляция ES6
 *
 *     Предполагаема структура директорий:
 *         css/           - папка с css
 *             c/         - папка со склеенным и уменьшенным css
 *             less/      - исходные файлы LESS
 *             maps/      - папка с map файлами для css
 *         js/            - папка с js
 *             c/         - папка со склеенным и уменьшенным js
 *             es6/       - папка с исходными файлами es6
 *             maps/      - папка с map файлами для js
 *
 *     Настройки имен файлов и директорий ниже
 */

var gulp = require('gulp'),                       // Сборщик Gulp
    gutil = require('gulp-util'),                 // Утилиты для Gulp плагинов
    plumber = require('gulp-plumber'),            // Обработчик ошибок сборщика
    path = require('path'),                       // Утилита для работы с путями
    less = require('gulp-less'),                  // Компилятор LESS
    watchLess = require('gulp-watch-less'),       // Наблюдатель за всеми includa'ми в файле
    concat = require('gulp-concat'),              // объединяет файлы в один бандл
    cleanCSS = require('gulp-clean-css'),         // сжимает css
    uglify = require('gulp-uglify'),              // Сжимает js
    rename = require('gulp-rename'),              // Переименовывает
    sourcemaps = require('gulp-sourcemaps');      // Генерация Source-maps


    // Для ES6
var browserify  = require('browserify'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    watchify = require('watchify'),
    assign = require('lodash.assign');

    // PostCSS
/*
var postcss = require('gulp-postcss'),
    postcss_mixins = require('postcss-mixins'),
    postcss_nested = require('postcss-nested'),
    postcss_simple_vars = require('postcss-simple-vars'),
    postcss_easings = require('postcss-easings'),
    postcss_import = require('postcss-import'),
    postcss_cssnext = require('postcss-cssnext'),
    postcss_hexrgba = require('postcss-hexrgba'),
    postcss_extend = require('postcss-extend'),
    postcss_units = require('postcss-units'),
    postcss_object_fit_images = require('postcss-object-fit-images'),  // плагин для полифила object-fit
    postcss_flexibility = require('postcss-flexibility'); // плагин для полифила flexbox
    */

var task_env = 'dev';  // Идентификатор цели запуска задачи (dev - для наблюдателей, prod - для финального билда)

// PostCSS
var postcss_plugins = [
    ['postcss-import'],
    ['postcss-mixins'],
    ['postcss-nested'],
    ['postcss-simple-vars'],
    ['postcss-hexrgba'],
    ['postcss-extend'],
    ['postcss-units'],
    ['postcss-cssnext', { browsers: ['> 0.5%', 'last 10 versions'] }],
    ['postcss-easings'],
    ['postcss-object-fit-images'],
    ['postcss-flexibility']
];

var postcss = require('gulp-postcss'),
    postcss_processors = [];

for (var i = 0; i < postcss_plugins.length; i ++) {
    var plugin = postcss_plugins[i].shift(),
        processor = require(plugin),
        args = postcss_plugins[i];
    postcss_processors.push(processor.apply(args));
}

// ---------------- CSS -------------------------------------------------

var cssPath = './css/';                               // Директория, где располагаются CSS-файлы
var cssMapPath = '/maps/';                            // Директория с map-файлами для css
var cssOutFile = 'style.css';                         // Название выходного файла css
var cssOutPath = path.join(cssPath, 'c');             // папка со склеенным и уменьшенным css. Файл имеет имя cssOutFile
var cssMapOutPath = path.join('..', cssMapPath, 'c'); // Директория с map-файлами для собранного css

var cssSourcemapsOptions = { sourceMappingURLPrefix: '/css' }; // Опции для настройка sourceMaps

// ---------------- LESS -------------------------------------------------

var lessPath = path.join(cssPath, 'less');        // Папка с LESS файлами
var mainLess = path.join(lessPath, 'style.less'); // Имя главного LESS файла проекта

// Настройки путей для LESS
var lessConfig = {
    paths: [
        lessPath,
        path.join(lessPath, 'parts'),
        path.join(lessPath, 'inc')
    ],
};

// ---------------- PostCSS --------------------------------------------------

var post_css_path = path.join(cssPath, 'postcss');        // Папка с PostCSS файлами
var post_css_main_file = path.join(post_css_path, 'style.pcss'); // Имя главного PostCSS файла проекта
var post_css_files_mask = path.join(post_css_path, '**', '*.pcss');

var clean_css_params = {
    format: 'keep-breaks'
};

// ---------------- JS --------------------------------------------------

var jsPath = './js/';                              // Директория JS
var jsMapPath = '/maps/';
var jsOutFile = 'c.js';                            // Название выходного js файла
var jsOutPath = path.join(jsPath, 'c');            // Папка для выходного js файла
var jsMapOutPath = path.join('..', jsMapPath);                     // Папка для map файлов

var jsSourcemapsOptions = { sourceMappingURLPrefix: '/js' };  // Опции для настройка sourceMaps

// Список js файлов для сборки. Меняется в зависимости от проекта
// названия файлов указываются от jsPath
var jsFiles = [

    // Системные библиотеки тоже соберем в один файл
    // 'lib/swfobject/swfobject.js',
    // 'lib/underscore.js',

    'lib/jquery3.js',
    'lib/jquery3/plugins/jquery-migrate-3.0.0.min.js',
    'lib/jquery/plugins/fancybox2/jquery.fancybox.pack.js',
    'lib/lodash.js',


    // Проектные библиотеки
    // 'fonts.js',
    // 'utils.js',
    // 'cookies.js',
    // 'proj.js',
    // 'proj/common.js',
    // 'proj/forms.js',
    // 'proj/page.js',
    // 'proj/targets.js',
    // 'main.js'
    'bundle.js',
    'main.js'
];

// ---------------- ES6 --------------------------------------------------

var es6_source_path = path.join(jsPath, 'es6');                    // Путь к исходникам
// var es6_source_mask = path.join(es6_source_path, '**', '*.js');    // Маска поиска иходников
var es6_bundle_name = 'bundle.js';                                 // Название основого файла сборки
var es6_bundle_path = path.join(es6_source_path, es6_bundle_name); // местоположение основного файла
var es6_destination = jsPath;                                      // Папка, куда нужно положить собранный файл

// параметры сборки ES6
// var es6_params = {
//     debug: true
// };

// ---------------- TASKS -----------------------------------------------


// function handleErrors() {
//     var args = Array.prototype.slice.call(arguments);
//     notify.onError({
//         title: 'Compile Error',
//         message: '<%= error.message %>'
//     }).apply(this, args);
//     this.emit('end'); // Keep gulp from hanging on this task
// }

function buildScript(watch) {
    var opts = assign(
        {},
        watchify.args,
        {
            entries: [ es6_bundle_path ],
            debug: ((task_env === 'prod') ? false : true)
        });

    var bundler = watch ? watchify(browserify(opts)) : browserify(opts);

    bundler.transform('babelify', {
        presets: ['es2015'],
        plugins: [['transform-es2015-classes', {loose: true}]]
    });

    function rebundle() {
        var stream = bundler.bundle();
        return stream.on('error', gutil.log.bind(gutil, 'Browserify Error'))
            // .on('error', gutil.log.bind(gutil, 'Browserify Error'))
            .pipe(source(es6_bundle_name))
            // optional, remove if you don't need to buffer file contents
            .pipe(buffer())
            // optional, remove if you dont want sourcemaps
            .pipe(sourcemaps.init({ loadMaps: true })) // loads map from browserify file
            // Add transformation tasks to the pipeline here.
            .pipe(sourcemaps.write(jsMapPath, jsSourcemapsOptions)) // writes .map file
            .pipe(gulp.dest(es6_destination));
    }
    bundler.on('update', function() {
        rebundle();
        gutil.log('Rebundle...');
    });
    bundler.on('log', gutil.log);
    return rebundle();
}

gulp.task('watch-es6', function() {
    return buildScript(true);
});

gulp.task('build-es6', function() {
    return buildScript(false);
});

var onError = function (err) {
    gutil.beep();
    console.log(err);
};

// Компиляция ES6 устаревшая
/*
gulp.task('build-es6', function () {
    if(task_env === 'prod'){
        es6_params.debug = false;
    }
    return browserify({
        entries: es6_bundle_path,
        debug: es6_params.debug
    })
    .transform('babelify', {
        presets: ['es2015'],
        plugins: [['transform-es2015-classes', {loose: true}]]
    })
    .bundle()
    .pipe(source(es6_bundle_name))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write(jsMapPath, jsSourcemapsOptions))
    .pipe(gulp.dest(es6_destination));
});

// Наблюдатель за ES6
gulp.task('watch-es6', function () {
    gulp.watch(es6_source_mask, ['build-es6']);
});
*/

// Сборка PostCSS файлов
gulp.task('postcss', function () {

    var resultMapPath = cssMapPath;
    var resultCssPath = cssPath;

    if(task_env === 'prod'){
        clean_css_params = { };
        resultMapPath = cssMapOutPath;
        resultCssPath = cssOutPath;
    }

/*
    var processors = [
        postcss_import(),
        postcss_mixins(),
        postcss_nested(),
        postcss_simple_vars(),
        postcss_hexrgba(),
        postcss_units(),
        postcss_cssnext({ browsers: [
            '> 0.5%',
            'last 10 versions'
        ] }),
        postcss_easings(),
        postcss_extend(),
        postcss_object_fit_images(),
        postcss_flexibility()
    ];
*/

    return gulp.src(post_css_main_file)
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(postcss(postcss_processors))
        .pipe(plumber.stop())
        .pipe(cleanCSS(clean_css_params))
        .pipe(rename(cssOutFile))
        .pipe(sourcemaps.write(resultMapPath, cssSourcemapsOptions))
        .pipe(gulp.dest(resultCssPath));
});

gulp.task('watch-postcss', function() {
    gulp.watch(post_css_files_mask, ['postcss']);
});

// Наблюдатель за less файлами
gulp.task('watch-less', function(){
    watchLess(mainLess, {
        name: 'LESS'
    })
    .pipe(sourcemaps.init())
    .pipe(plumber({
        errorHandler: onError
    }))
    .pipe(less(lessConfig))
    .pipe(plumber.stop())
    .pipe(cleanCSS({ keepBreaks: true,
                     restructuring: false }))
    .pipe(rename(cssOutFile))
    .pipe(sourcemaps.write(cssMapPath, cssSourcemapsOptions))
    .pipe(gulp.dest(cssPath));
});

// Установка окружения для сборки проекта
gulp.task('production', function(){
    task_env = 'prod';
});

// Сборка и минимизация CSS через PostCss
gulp.task('build-css-with-postcss', ['production', 'postcss']);

// Сборка и минимизация CSS из LESS
gulp.task('build-css-with-less', function(){
    gulp.src(mainLess)
        .pipe(sourcemaps.init())
        .pipe(less(lessConfig))
        .pipe(cleanCSS())
        .pipe(rename(cssOutFile))
        .pipe(sourcemaps.write(cssMapOutPath, cssSourcemapsOptions))
        .pipe(gulp.dest(cssOutPath));
});

// Сборка и минимизация JS
gulp.task('build-js', ['production', 'build-es6'], function(){
    gulp.src(jsFiles, { cwd: jsPath })
        .pipe(sourcemaps.init())
        .pipe(concat(jsOutFile))
        .pipe(uglify())
        .pipe(sourcemaps.write(jsMapOutPath, jsSourcemapsOptions))
        .pipe(gulp.dest(jsOutPath));
});

// Сборка проекта
// gulp.task('build', ['build-css-with-less', 'build-js']);
gulp.task('build', ['build-css-with-postcss', 'build-js']);


